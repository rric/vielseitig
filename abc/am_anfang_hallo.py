# Am Anfang jedes Programmierkurses gibt's ein "Hallo, Welt!"-Programm
# Copyright 2022--2024 Roland Richter                  [Mu: Pygame Zero]

# Erzeuge ein rechteckiges 600×400 Fenster
WIDTH = 600
HEIGHT = 400

def draw():
    # Setze den Hintergrund auf schwarz
    screen.fill("black")

    # Möchtest du eine andere Farbe für den Hintergrund verwenden?
    # -> https://www.pygame.org/docs/ref/color_list.html

    # Begrüße alle in der Mitte des Fensters
    screen.draw.text("Hallo, Welt!", (300, 200))

    # ... und eine andere Farbe für den Text?
    # -> https://pygame-zero.readthedocs.io/en/stable/ptext.html

# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
