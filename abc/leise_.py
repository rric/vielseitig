# Lerne einiges darüber, wie man Listen von Zahlen in Python erzeugt.
# Copyright 2023-2024 Roland Richter                      [Mu: Python 3]

# Wenn du eine Liste erzeugen willst, die die Zahlen 0, 40, 80, ..., 400
# enthält, gibt es mehrere Arten, das zu tun:
# 1. Du kannst einfach eine Liste mit allen Elementen aufschreiben:
zahlen_a = [0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 400]

# ... wirklich mühsam zu tippen, nicht wahr? Es gibt was besseres.

# 2. Python bietet eine weitere Art, Zahlen aufzuzählen:
# die "for"-Schleife mit der Funktion range(start, stop, step), wobei
# start die Zahl ist, mit der die Aufzählung beginnen soll,
# stop die Zahl ist, _vor_ der gestoppt werden soll,
# step die Schrittweite angibt.
# Häufig passiert hier ein Fehler, weil `stop` NICHT enthalten ist.
# ÄNDERE den Code so, dass alle Zahlen 0, 40, ... 400 entstehen:
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
zahlen_c = []
for zahl in range(0, 400, 40):
    zahlen_c.append(zahl)
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# 3. Zwei der Argumente von range(start, stop, step) sind optional:
# start ist die Zahl, mit der begonnen werden soll -- optional (default: 0),
# stop ist die Zahl, mit der gestoppt werden soll (nicht enthalten) -- erforderlich,
# step ist die Angabe der Schrittweite - optional (default: 1).
# Der Code "for zahl in range(10)" bedeutet also "for zahl in range(0, 10, 1)",
# und das wiederum bedeutet "Zähle 10 Zahlen auf, von 0 bis 9".
# ÄNDERE den Code so, dass alle Zahlen 0, 40, ... 400 entstehen:
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
zahlen_d = []
for zahl in range(10):
    zahlen_d.append(40 * zahl)
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# 4. Es gibt eine noch kürzere Art: die sogenannt list comprehension.
# ÄNDERE den (hoffentlich offensichtlichen) Berechnungsfehler.
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
zahlen_e = [42 * zahl for zahl in range(11)]
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# 5. Du bist dran!
# Erzeuge [0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165]
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
zahlen_f = []
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# 6. ... und gleich noch einmal:
# Erzeuge die Liste [2, 6, 10, 14, 18, 22, 26, 30, 34]
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
zahlen_g = []
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

# Übrigens: man kann nicht nur rauf, sondern auch runter zählen.
# 7. Erzeuge den Countdown [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
# ↓---------↓---------↓---------↓---------↓---------↓---------↓
countdown = []
# ↑---------↑---------↑---------↑---------↑---------↑---------↑


# 🖐 HALT! Änderungen unter dieser Zeile sind verboten!
# ↑---------↑---------↑---------↑---------↑---------↑---------↑

zahlen40 = [0, 40, 80, 120, 160, 200, 240, 280, 320, 360, 400]
zahlen15 = [0, 15, 30, 45, 60, 75, 90, 105, 120, 135, 150, 165]
zahlen34 = [2, 6, 10, 14, 18, 22, 26, 30, 34]

assert zahlen_c == zahlen40, (
    "\n  zahlen_c ist " + str(zahlen_c) + "\n  sollte aber " + str(zahlen40) + " sein."
)
assert zahlen_d == zahlen40, (
    "\n  zahlen_d ist " + str(zahlen_d) + "\n  sollte aber " + str(zahlen40) + " sein."
)
assert zahlen_e == zahlen40, (
    "\n  zahlen_e ist " + str(zahlen_e) + "\n  sollte aber " + str(zahlen40) + " sein."
)
assert zahlen_f == zahlen15, (
    "\n  zahlen_f ist " + str(zahlen_f) + "\n  sollte aber " + str(zahlen15) + " sein."
)
assert zahlen_g == zahlen34, (
    "\n  zahlen_g ist " + str(zahlen_g) + "\n  sollte aber " + str(zahlen34) + " sein."
)
assert countdown == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1], (
    "\n  countdown ist " + str(countdown)
    + "\n  sollte aber " + str([10, 9, 8, 7, 6, 5, 4, 3, 2, 1]) + " sein."
)
print("+---------+---------+---------+---------+---------+---------+")
input("Du hast es geschafft! Bravo! 👏")

# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
