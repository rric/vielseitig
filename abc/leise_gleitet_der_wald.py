# Eine Menge an Bäumen ziehen vorbei ...
# Copyright 2023 Roland Richter                        [Mu: Pygame Zero]

import random

# Erzeuge ein rechteckiges 800×500 Fenster
TITLE = "Leise gleitet der Wald"
WIDTH = 800
HEIGHT = 500
frame_count = 0

# Erzeuge die Positionen, an denen Bäume gemalt werden:
# - horizontal alle 80 Pixel
# - vertikal zwischen 440 und 460
# PROBIERE, mehr Bäume hinzu zu fügen, und die Bäume enger zueinander zu stellen
wood = []
for num in range(12):
    tree_x = 80 * num
    tree_y = random.randint(440, 460)
    wood.append((tree_x, tree_y))


def draw():
    global frame_count
    frame_count += 1

    screen.fill("skyblue2")

    # Male Schnee in den unteren Teil des Fensters
    screen.draw.filled_rect(Rect(0, 0.8 * HEIGHT, WIDTH, 0.2 * HEIGHT), "snow")

    # Male die Sonne
    screen.draw.filled_circle((150, 70), 45, "yellow")

    # Male alle Bäume in der "Wald"-Liste
    for tree_pos in wood:
        draw_tree(tree_pos)


def update():
    global wood

    # ⭠: flieg nach links = alle Bäume rücken 3 Pixel nach RECHTS
    if keyboard.left:
        wood = [(pos[0] + 3, pos[1]) for pos in wood]
    # ⭢: flieg nach rechts = alle Bäume rücken 3 Pixel nach LINKS
    elif keyboard.right:
        wood = [(pos[0] - 3, pos[1]) for pos in wood]


def draw_tree(pos):
    """Male einen Baum an der gegebenen Position."""
    x, y = pos
    screen.blit("kenney_platformer-art-winter/tiles/pinesapling.png", (x - 35, y - 70))


# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
